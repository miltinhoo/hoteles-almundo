import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConstantesService {
  public PATH_BUSCAR_IMG = 'assets/img/iconos/filtros/search.svg';
  public PATH_ESTRELLA = 'assets/img/iconos/filtros/star.svg';
  public PATH_COMODIDADES = 'assets/img/iconos/comodidades/';
  public PATH_IMG_HOTELES = 'assets/img/hoteles/';
  public EXT_COMODIDADES_IMG = '.svg';
  public NUM_ESTRELLAS_CHECK = [5, 4, 3, 2, 1];
  public MSG_HOTELES_NO_EXISTEN = 'No existen Hoteles con los filtros seleccionados';
  public MSG_ERR_CONSULTA_HOTEL = 'Se presentaron errores en el servicio $VAL1, por favor intente mas tarde.';

  constructor() { }
}
