import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { catchError, tap } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Hotel } from '../../model/hotel.model';
import { ConstantesService } from '../constantes.service';

const headers = new HttpHeaders(
    {'Content-Type': 'application/json'}
    );


@Injectable({
  providedIn: 'root'
})
export class HotelesService {

  hoteles: any[] = [
    {
      'id': '249942',
      'name': 'Hotel Stefanos',
      'stars': 3,
      'price': 994.18,
      'image': '4900059_30_b.jpg',
      'amenities': [
        'safety-box',
        'nightclub',
        'deep-soaking-bathtub',
        'beach',
        'business-center'
      ]
    },
    {
      'id': '161901',
      'name': 'Hotel Santa Cruz',
      'stars': 3,
      'price': 1267.57,
      'image': '6623490_6_b.jpg',
      'amenities': [
        'nightclub',
        'business-center',
        'bathtub',
        'newspaper',
        'restaurant'
      ]
    },
    {
      'id': '161914',
      'name': 'NM Lima Hotel',
      'stars': 4,
      'price': 1445.5,
      'image': '981018_26_b.jpg',
      'amenities': [
        'business-center',
        'nightclub',
        'deep-soaking-bathtub',
        'fitness-center',
        'garden'
      ]
    },
    {
      'id': '208599',
      'name': 'El Golf Hotel Boutique',
      'stars': 4,
      'price': 1513.45,
      'image': '1191401_110_b.jpg',
      'amenities': [
        'safety-box',
        'bathrobes',
        'business-center',
        'beach-pool-facilities',
        'restaurant'
      ]
    },
    {
      'id': '236309',
      'name': 'Casa Andina Select Miraflores',
      'stars': 4,
      'price': 1545.46,
      'image': '2a5bb415_b.jpg',
      'amenities': [
        'coffe-maker',
        'safety-box',
        'restaurant',
        'garden',
        'children-club'
      ]
    },
    {
      'id': '161899',
      'name': 'Radisson Hotel Decapolis Miraflores',
      'stars': 4,
      'price': 2314.61,
      'image': '46512_29_b.jpg',
      'amenities': [
        'bathtub',
        'separate-bredroom',
        'safety-box',
        'business-center',
        'beach-pool-facilities'
      ]
    },
    {
      'id': '104074',
      'name': 'Antigua Miraflores Hotel',
      'stars': 3,
      'price': 1862.16,
      'image': '480835_73_b.jpg',
      'amenities': [
        'restaurant',
        'separate-bredroom',
        'business-center',
        'bathrobes',
        'beach'
      ]
    },
    {
      'id': '512559',
      'name': 'Casa Falleri Boutique Hotel',
      'stars': 3,
      'price': 1634.03,
      'image': '11908954_150_b.jpg',
      'amenities': [
        'nightclub',
        'garden',
        'coffe-maker',
        'kitchen-facilities',
        'deep-soaking-bathtub'
      ]
    }
  ];
  private hotelsUrl = 'http://localhost:8001/hotels';
  private hotelsTotalPageUrl = 'http://localhost:8001/hotels/numberHotels';
  constructor(
    private http: HttpClient,
    private constantesService: ConstantesService
  ) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      if (error.status === 404) {
        return of(result as T);
      }
      throw new Error(this.constantesService.MSG_ERR_CONSULTA_HOTEL.replace('$VAL1', operation));
    };
  }

  getHotelsParams(nombre: string, estrellas: number[]) {
    let params = new HttpParams();
    if (nombre) {
      params = params.append('hotelName', nombre);
    }
    if (estrellas && estrellas.length > 0) {
      estrellas.forEach(numeroEstrella => {
        let key;
        if (numeroEstrella === 1) {
          key = 'starOne';
        } else if (numeroEstrella === 2) {
          key = 'starTwo';
        } else if (numeroEstrella === 3) {
          key = 'starThree';
        } else if (numeroEstrella === 4) {
          key = 'starFour';
        } if (numeroEstrella === 5) {
          key = 'starFive';
        }
        if (key) {
          params = params.append(key, 'true');
        }
      });
    }
    return params;
  }

  consultarHoteles(nombre: string, estrellas: number[], pagina: number): Observable<any> {
    const params = this.getHotelsParams(nombre, estrellas);

    return  this.http.get<any[]>(this.hotelsUrl + '/' + pagina, {headers, params}).pipe(
      tap(
        hoteles => {
          return hoteles;
        }),
      catchError(this.handleError('consultarHoteles', []))
    );
  }

  consultarNumeroPaginas(numeroHoteles: number): Observable<any> {
    return  this.http.get<any[]>(this.hotelsTotalPageUrl + '/' + numeroHoteles, {headers}).pipe(
      tap(
        hoteles => {
          return hoteles;
        }),
      catchError(this.handleError('consultarNumeroPaginas', []))
    );
  }

}
