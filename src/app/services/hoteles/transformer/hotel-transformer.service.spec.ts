import { TestBed, inject } from '@angular/core/testing';

import { HotelTransformerService } from './hotel-transformer.service';

describe('HotelTransformerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotelTransformerService]
    });
  });

  it('should be created', inject([HotelTransformerService], (service: HotelTransformerService) => {
    expect(service).toBeTruthy();
  }));
});
