import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HotelBackToFrontService {
  constructor() {}

  hotelsNumberToNumeroHoteles(rep: any) {
    if (rep && rep.response && rep.response.totalHotelsNumber) {
      return rep.response.totalHotelsNumber;
    }
    return 0;
  }

  hotelsToHoteles(resp: any) {
    if (resp && resp.response && resp.response.hotels) {
      return resp.response.hotels;
    }
    return [];
  }

  hotelsTotalNumberToNumeroHoteles(resp: any): number {
    if (resp && resp.response && resp.response.hotelsTotalNumber) {
      return resp.response.hotelsTotalNumber;
    }
    return 0;
  }
}
