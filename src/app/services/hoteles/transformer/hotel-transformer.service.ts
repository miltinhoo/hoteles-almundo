import { Injectable } from '@angular/core';
import { Hotel } from '../../../model/hotel.model';

@Injectable({
  providedIn: 'root'
})
export class HotelTransformerService {

  constructor() { }

  transformerHoteles(hoteles: any[]) {
    let hotelesLista: any[] =  [];
    hoteles.forEach(x => {
      hotelesLista.push( this.mapperHotel(x));
    });
    return hotelesLista;
  }

  private mapperHotel(hotel: any): Hotel {

    const hotelReturn = {
      id: hotel.id,
      nombre: hotel.name,
      numeroEstrellas: hotel.stars,
      precio: hotel.price,
      imagenPath: hotel.image,
      comodidades: hotel.amenities
    };
    return hotelReturn;
  }
}
