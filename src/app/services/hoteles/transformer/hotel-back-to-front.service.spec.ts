import { TestBed, inject } from '@angular/core/testing';

import { HotelBackToFrontService } from './hotel-back-to-front.service';

describe('HotelBackToFrontService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HotelBackToFrontService]
    });
  });

  it('should be created', inject([HotelBackToFrontService], (service: HotelBackToFrontService) => {
    expect(service).toBeTruthy();
  }));
});
