import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { APP_ROUTING } from './app.routes';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { FiltroComponent } from './components/filtro/filtro.component';
import { FiltroNombreHotelComponent } from './components/filtro/filtro-nombre-hotel/filtro-nombre-hotel.component';
import { FiltroEstrellaComponent } from './components/filtro/filtro-estrella/filtro-estrella.component';
import { HotelesComponent } from './components/hoteles/hoteles.component';
import { ResumenHotelesComponent } from './components/hoteles/resumen-hoteles/resumen-hoteles.component';
import { ConstantesService } from './services/constantes.service';
import { HotelesService } from './services/hoteles/hoteles.service';
import { HotelTransformerService } from './services/hoteles/transformer/hotel-transformer.service';
import { PaginacionComponent } from './components/shared/paginacion/paginacion.component';
import { HttpClientModule } from '@angular/common/http';
import { HotelBackToFrontService } from './services/hoteles/transformer/hotel-back-to-front.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    FiltroComponent,
    FiltroNombreHotelComponent,
    FiltroEstrellaComponent,
    HotelesComponent,
    ResumenHotelesComponent,
    PaginacionComponent
  ],
  imports: [
    BrowserModule,
    APP_ROUTING,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    ConstantesService,
    HotelesService,
    HotelTransformerService,
    HotelBackToFrontService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
