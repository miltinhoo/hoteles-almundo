export interface Hotel {
    id: string;
    nombre: string;
    numeroEstrellas: number;
    precio: number;
    imagenPath: string;
    comodidades: string[];
}
