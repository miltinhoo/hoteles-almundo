import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResumenHotelesComponent } from './resumen-hoteles.component';

describe('ResumenHotelesComponent', () => {
  let component: ResumenHotelesComponent;
  let fixture: ComponentFixture<ResumenHotelesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResumenHotelesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResumenHotelesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
