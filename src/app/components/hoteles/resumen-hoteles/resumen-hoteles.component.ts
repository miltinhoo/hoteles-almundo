import { Component, OnInit, Input } from '@angular/core';
import { ConstantesService } from '../../../services/constantes.service';
import { Hotel } from '../../../model/hotel.model';

@Component({
  selector: 'app-resumen-hoteles',
  templateUrl: './resumen-hoteles.component.html',
  styleUrls: ['./resumen-hoteles.component.css']
})
export class ResumenHotelesComponent implements OnInit {

  @Input() hotel: Hotel;
  estrellas: number[];
  imagen: string;

  PATH_COMODIDADES: string;
  PATH_ESTRELLA: string;
  EXT_COMODIDAD_IMG: string;

  constructor(private constantesService: ConstantesService) {
    this.setPathEstrella(constantesService.PATH_ESTRELLA);
    this.setPathComodidad(constantesService.PATH_COMODIDADES);
    this.setExtComodidadImg(constantesService.EXT_COMODIDADES_IMG);
  }

  ngOnInit() {
    this.estrellas = this.setArrayEstrellas(this.hotel.numeroEstrellas);
  }

  getImagenHotel(pathImagen: string): string {
    return this.constantesService.PATH_IMG_HOTELES + pathImagen;
  }

  verHotel(idHotel: number) {
    console.log('Id Hotel: ' + idHotel);
  }

  setArrayEstrellas(numeroEstrellas: number): number[] {
    return Array(numeroEstrellas).fill(0);
  }

  setPathEstrella(pathEstrellas) {
    this.PATH_ESTRELLA = pathEstrellas;
  }

  setPathComodidad(pathComodidades) {
    this.PATH_COMODIDADES = pathComodidades;
  }

  setExtComodidadImg(extComodidadImg) {
    this.EXT_COMODIDAD_IMG = extComodidadImg;
  }

  getImgComodidad(nombreImagenComodidad: string): string {
    return this.PATH_COMODIDADES + nombreImagenComodidad + this.EXT_COMODIDAD_IMG;
  }
}
