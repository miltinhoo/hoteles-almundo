import { Component, OnInit, OnDestroy } from '@angular/core';
import { Hotel } from '../../model/hotel.model';
import { HotelesService } from '../../services/hoteles/hoteles.service';
import { HotelBackToFrontService } from '../../services/hoteles/transformer/hotel-back-to-front.service';
import { HotelTransformerService } from '../../services/hoteles/transformer/hotel-transformer.service';
import { ConstantesService } from '../../services/constantes.service';
import { Observable, Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-hoteles',
  templateUrl: './hoteles.component.html',
  styleUrls: ['./hoteles.component.css']
})
export class HotelesComponent implements OnInit, OnDestroy {
  hoteles: Hotel[];
  nombre: string;
  estrellas: number[];
  paginaActual: number;
  paginasTotales: number;
  mostrar = false;
  mensajeError: string;
  private subscription: Subscription;

  constructor(
    private hotelesService: HotelesService,
    private hotelBackToFrontService: HotelBackToFrontService,
    private hotelTransformerService: HotelTransformerService,
    private constantesService: ConstantesService
  ) {
    this.subscription = new Subscription();
    this.estrellas = [];
    this.paginaActual = 1;
  }

  ngOnInit() {
    const subscripcion = this.consultaHoteles().subscribe(
      _ => this.consultarNumeroPaginas(_)
    );
    this.subscription.add(subscripcion);
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  reiniciarPaginaResumen() {
    this.mostrar = false;
    this.paginaActual = 1;
  }

  getMensajeNoExiste(): string {
    return this.mensajeError || this.constantesService.MSG_HOTELES_NO_EXISTEN;
  }

  consultarHotelesPorNombre(nombre: string) {
    this.nombre = nombre;
    this.reiniciarPaginaResumen();
    const subscripcion = this.consultaHoteles().subscribe(
      _ => this.consultarNumeroPaginas(_)
    );
    this.subscription.add(subscripcion);
  }

  consultarHotelesPorEstrella(estrellasFiltro: number[]) {
    this.estrellas = estrellasFiltro;
    this.reiniciarPaginaResumen();
    const subscripcion = this.consultaHoteles().subscribe(
      _ => this.consultarNumeroPaginas(_)
    );
    this.subscription.add(subscripcion);
  }

  consultarHotelesPorPagina(pagina: number) {
    this.paginaActual = pagina;
    const subscripcion = this.consultaHoteles().subscribe(
      _ => this.consultarNumeroPaginas(_)
    );
    this.subscription.add(subscripcion);
  }

  consultaHoteles(): Observable<any> {
    this.mensajeError = undefined;
    return new Observable((observer) => {
      this.hotelesService.consultarHoteles(this.nombre, this.estrellas, this.paginaActual).subscribe(
        _ => {
          this.hoteles = this.hotelTransformerService.transformerHoteles(
            this.hotelBackToFrontService.hotelsToHoteles(_)
          );
          const numeroTotalHoteles = this.hotelBackToFrontService.hotelsTotalNumberToNumeroHoteles(_);
          this.mostrar = true;
          observer.next(numeroTotalHoteles);
        },
        error => {
          this.mensajeError = error.message;
          observer.error();
        }
      );
    });
  }

  consultarNumeroPaginas(numeroHoteles: number) {
    if (numeroHoteles > 0) {
      this.hotelesService.consultarNumeroPaginas(numeroHoteles).subscribe(
        _ =>
          this.paginasTotales = this.hotelBackToFrontService.hotelsNumberToNumeroHoteles(_)
        ,
        error => this.mensajeError = error.message
      );
    }
  }

}
