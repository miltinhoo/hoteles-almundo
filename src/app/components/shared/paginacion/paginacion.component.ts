import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.css']
})
export class PaginacionComponent implements OnInit {
  @Input() paginaActual: number;
  @Input() paginasTotales: number;
  @Output() paginaSeleccionada: EventEmitter<number>;
  paginasMostradas: number[] = [];
  paginaInicial: number;

  constructor() {
    this.paginasMostradas = [
      1, 2, 3
    ];
    this.paginaSeleccionada = new EventEmitter();
  }

  ngOnInit() {
    this.paginaInicial = 1;
  }

  mostrarPaginaAnterior(): string {
    if (this.paginaActual && this.paginaActual > this.paginaInicial) {
      return 'enabled';
    }
    return 'disabled';
  }

  mostrarPaginaSiguiente(): string {
    if (this.paginaActual && this.paginaActual < this.paginasTotales) {
      return 'enabled';
    }
    return 'disabled';
  }

  mostrarPaginaSiguienteNum(pagina): string {
    if (pagina > this.paginasTotales) {
      return 'disabled';
    }
    return 'enabled';
  }

  paginaActiva(pagina): string {
    if (pagina === this.paginaActual) {
      return 'active';
    }
    return 'inactive';
  }

  seleccionarPaginaSiguiente() {
    const index = this.paginaActual === this.paginaInicial ? 1 : 0;
    this.seleccionarPagina(this.paginaActual + 1, index);
  }

  seleccionarPaginaAnterior() {
    const index = this.paginaActual === this.paginasTotales ? 1 : 0;
    this.seleccionarPagina(this.paginaActual - 1, index);
  }

  seleccionarPagina(paginaSeleccionada: number, index: number) {
    this.modificarArregloPaginas(paginaSeleccionada, index);
    this.llamarServicioHoteles(paginaSeleccionada);
  }

  modificarArregloPaginas(paginaSeleccionada: number, index: number) {
    if (
      paginaSeleccionada !== this.paginasTotales &&
      paginaSeleccionada !== this.paginaInicial &&
      index !== 1
    ) {
      if (paginaSeleccionada > this.paginaActual) {
        this.paginasMostradas.shift();
        this.paginasMostradas.push(paginaSeleccionada + 1);
      } else if (paginaSeleccionada < this.paginaActual) {
        this.paginasMostradas.pop();
        this.paginasMostradas.unshift(paginaSeleccionada - 1);
      }
    }
  }

  llamarServicioHoteles(paginaSeleccionada: number) {
    this.paginaSeleccionada.emit(paginaSeleccionada);
  }

}
