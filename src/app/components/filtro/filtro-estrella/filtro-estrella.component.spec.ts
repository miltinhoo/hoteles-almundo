import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroEstrellaComponent } from './filtro-estrella.component';

describe('FiltroEstrellaComponent', () => {
  let component: FiltroEstrellaComponent;
  let fixture: ComponentFixture<FiltroEstrellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroEstrellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroEstrellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
