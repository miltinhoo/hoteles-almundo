import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConstantesService } from '../../../services/constantes.service';

@Component({
  selector: 'app-filtro-estrella',
  templateUrl: './filtro-estrella.component.html',
  styleUrls: ['./filtro-estrella.component.css']
})
export class FiltroEstrellaComponent implements OnInit {

  estrellasCheck: any[];
  PATH_ESTRELLA: string;
  numeroEstrellasCheck: number[];
  @Output() numeroEstrellasFiltro: EventEmitter<number[]>;

  constructor(private constantesService: ConstantesService) {
    this.numeroEstrellasCheck = [];
    this.numeroEstrellasFiltro = new EventEmitter();
    this.setPathEstrella(constantesService.PATH_ESTRELLA);
    this.createCheckEstrellas();
   }

  ngOnInit() {
  }

  createCheckEstrellas() {
    this.estrellasCheck = [
      {
        checked: false,
        numero: [1, 2, 3, 4, 5]
      },
      {
        checked: false,
        numero: [1, 2, 3, 4]
      },
      {
        checked: false,
        numero: [1, 2, 3]
      },
      {
        checked: false,
        numero: [1, 2]
      },
      {
        checked: false,
        numero: [1]
      }
    ];
  }

  setPathEstrella(pathEstrellas) {
    this.PATH_ESTRELLA = pathEstrellas;
  }

  checkEvento(estrellas: any, evento: any) {
    const numEstrellas = estrellas.length;
    const index: number = this.numeroEstrellasCheck.indexOf(numEstrellas);

    if (evento) {
      this.numeroEstrellasCheck.push(numEstrellas);
    } else if (index !== -1) {
        this.numeroEstrellasCheck.splice(index, 1);
    }
    this.numeroEstrellasFiltro.emit(this.numeroEstrellasCheck);
  }

  checkEventoTodasLasEstrellas(evento: any) {
    if (evento) {
      this.estrellasCheck.forEach(x => x.checked = true);
      this.numeroEstrellasCheck = this.constantesService.NUM_ESTRELLAS_CHECK;
      this.numeroEstrellasFiltro.emit(this.numeroEstrellasCheck);
    } else {
      this.numeroEstrellasCheck = [];
      this.estrellasCheck.forEach(x => x.checked = false);
    }
  }
}
