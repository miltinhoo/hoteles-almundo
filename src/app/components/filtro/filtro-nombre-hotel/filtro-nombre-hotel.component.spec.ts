import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroNombreHotelComponent } from './filtro-nombre-hotel.component';

describe('FiltroNombreHotelComponent', () => {
  let component: FiltroNombreHotelComponent;
  let fixture: ComponentFixture<FiltroNombreHotelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroNombreHotelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroNombreHotelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
