import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ConstantesService } from '../../../services/constantes.service';
import { Hotel } from '../../../model/hotel.model';

@Component({
  selector: 'app-filtro-nombre-hotel',
  templateUrl: './filtro-nombre-hotel.component.html',
  styleUrls: ['./filtro-nombre-hotel.component.css']
})
export class FiltroNombreHotelComponent implements OnInit {

  PATH_BUSCAR_IMG: string;
  hoteles: Hotel[];
  @Output() nombreHotelBusqueda: EventEmitter<string>;

  constructor(
    private constantesService: ConstantesService
  ) {
    this.nombreHotelBusqueda = new EventEmitter();
    this.setPathBuscarImg(constantesService.PATH_BUSCAR_IMG);
  }

  ngOnInit() {
  }

  setPathBuscarImg(pathBuscarImg) {
    this.PATH_BUSCAR_IMG = pathBuscarImg;
  }

  consultarHotelesPorNombre(nombre: string) {
    this.nombreHotelBusqueda.emit(nombre);
  }

}
