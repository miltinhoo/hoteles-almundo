import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.css']
})
export class FiltroComponent implements OnInit {
  @Output() nombreBusquedaParametro: EventEmitter<string>;
  @Output() estrellasFiltro: EventEmitter<number[]>;

  constructor() {
    this.nombreBusquedaParametro = new EventEmitter();
    this.estrellasFiltro = new EventEmitter();
  }

  ngOnInit() {
  }

  parametroNombreBusqueda(nombre: string) {
    this.nombreBusquedaParametro.emit(nombre);
  }

  parametroEstrellaBusqueda(estrellas: number[]) {
    this.estrellasFiltro.emit(estrellas);
  }

}
